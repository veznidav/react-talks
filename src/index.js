import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { Rendering } from "./Rendering";
import { RenderApp } from "./rendering/RenderApp";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<RenderApp />);
