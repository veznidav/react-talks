import { useEffect, useState } from "react";

function manipulateArray(arr, count) {
    if (arr.length < count) {
      // Pad the array with "999" up to 5 items
      const paddingCount = count - arr.length;
      arr = arr.concat(Array(paddingCount).fill({ title: "N/A" }));
    } else {
      // Get only the first 5 items from the array
      arr = arr.slice(0, count);
    }
    
    return arr;
  }

export function Todos({ count }) {
  const [data, setData] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const result = await fetch("https://gorest.co.in/public/v2/todos");
        const jsoned = await result.json();
        setData(jsoned);
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
  }, []);

  const dataTransformed = manipulateArray(data || [], count)

  const items = dataTransformed.length > 0
    ? dataTransformed.map((item) => <li key={item.id}>- {item.title}</li>)
    : null;

  return (
    <div>
      {isLoading && "loading..."}
      <ul>{items}</ul>
    </div>
  );
}
