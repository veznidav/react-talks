import { useState } from "react";

function Inner() {
    const [count, setCount] = useState(0);

    return (
        <div>
            <button onClick={() => setCount(count +1)}>+1</button>
            count: {count}
        </div>
    )
}

export function Rendering() {

    const [a, setA] = useState(0);



    return <div>
        <button onClick={() => setA(a +1)}>Render</button>
        <Inner key={a} />
    </div>
}