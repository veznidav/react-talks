import { useState } from "react";

export function RenderCommit() {
  const [list, setList] = useState(["Albert", "Tesco", "Penny"]);
  const [text, setText] = useState("");

  return (
    <div>
      <input onChange={(e) => setText(e.target.value)} value={text} />
      <button
        onClick={() => {
          const arr = [...list, text];  
          setList(arr);
          setText("");
        }}
      >
        Add
      </button>
      <div>
        <ul>
            {list.map((item, index) => <li key={index}>{item}</li>)}
        </ul>
      </div>
    </div>
  );
}
