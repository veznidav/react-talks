import { useEffect, useState } from "react";

function Inner() {
  useEffect(() => {
    alert("Inner mounts");
  }, []);

  return <div>Inner component</div>;
}

export function Key() {
  const [count, setCount] = useState(0);
  const k = Math.random() * 1000;
  //const k = 1000;
  return (
    <div>
      {count} <br />
      <button onClick={() => setCount((prev) => prev + 1)}>+1</button>
      <br />
      <Inner key={k} />
    </div>
  );
}

export function Cmp() {
  const [count, setCount] = useState(0);

  if (count % 2 === 0) {
    return (
      <div>
        {count} <br />
        <button onClick={() => setCount((prev) => prev + 1)}>+1</button>
        <br />
        <button onClick={() => setCount((prev) => prev + 2)}>+2</button>
        <br />
        <Inner />
      </div>
    );
  }

  return (
    <div>
      {count} <br />
      <button onClick={() => setCount((prev) => prev + 1)}>+1</button>
      <br />
      <button onClick={() => setCount((prev) => prev + 2)}>+2</button>
      <br />
      <Inner />
    </div>
  );
}
