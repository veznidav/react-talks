import { Deferred, Transition } from "./Deferred";
import { InfiniteLoops } from "./InfiniteLoops";
import { Cmp, Key } from "./Key";
import { RenderCascade } from "./RenderCascade";
import { RenderCommit } from "./RenderCommit";

export function RenderApp() {
  return (
    <div>
      Rendering Demo
      {/* <RenderCommit /> */}
      {/* <RenderCascade /> */}
      {/* <Cmp /> */}
      {/* <InfiniteLoops /> */}
      {/* <Deferred /> */}
      {/* <Transition /> */}
    </div>
  );
}
