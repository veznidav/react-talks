import { useDeferredValue, useMemo, useState, useTransition } from "react";

const largeList = Array(100000).fill(11);

export function Deferred() {
  const [count, setCount] = useState("");
  const deferredCount = useDeferredValue(count)
  //const deferredCount = count;

  console.log('deferredCount', deferredCount)
  console.log('count', count)

  const list = useMemo(() => {
    return largeList.map((item) => item * deferredCount);
  }, [deferredCount]);

  function handleChange(e) {
    setCount(e.target.value);
  }

  return (
    <>
      <input type="text" value={count} onChange={handleChange} />
      {list.map((item) => (
        <div>{item}</div>
      ))}
    </>
  );
}

export function Transition() {
  const [count, setCount] = useState("");
  const [count2, setCount2] = useState("");
  const [isPending, startTransition] = useTransition();

  const list = useMemo(() => {
    return largeList.map((item) => item * count2);
  }, [count2]);

  function handleChange(e) {
    setCount(e.target.value);
    startTransition(() => setCount2(e.target.value));
  }

  return (
    <>
      Transition
      <input type="text" value={count} onChange={handleChange} />
      {isPending ? "pending..." : list.map((item) => <div>{item}</div>)}
    </>
  );
}
