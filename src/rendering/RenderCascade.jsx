import { memo, useState } from "react"

function A() {
    console.log('render A')
    return <div>Component: A <br /><B /></div>
}

const MemoA = memo(A);

function B() {
    console.log('render B')
    return <div>Component: B <br /><C /></div>
}


function C() {
    console.log('render C')
    return <div>Component: C <br /><D /></div>
}


function D() {
    console.log('render D')
    return <div>Component: D <br /></div>
}

export function RenderCascade() {
    const [state, setState] = useState(0);
  return (
    <div>
        <button onClick={() => setState((prevVal) => prevVal + 1)}>+1</button><br />
        {state}<br />
        <MemoA />
    </div>
  );
}
