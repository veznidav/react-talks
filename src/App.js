import { useState } from "react";
import { Todos } from "./Todos";

let isAdmin = true;
let number = 0;

const items = ["Tom", "Bob", "Alice", "Jerry"];

function Board(props) {
  return <div className="font-bold m-9">Number of clicks: {props.number}</div>;
}

function App() {
  const [clicks, setClicks] = useState(0);

  const handleClick = () => {
    setClicks(clicks + 1);
    // number++;
  };

  return (
    <>
      <div className="text-3xl font-bold text-center">
        hello mr {isAdmin ? "Admin" : "User"}
      </div>
      <div>cau</div>
      <ul>
        {items.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
      <button
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ml-2 mt-6"
        onClick={() => setClicks(clicks + 1)}
      >
        +
      </button>
      <button
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ml-2 mt-6"
        onClick={() => setClicks(clicks - 1)}
      >
        -
      </button>
      <Board number={clicks} />
      <button onClick={() => alert(number)}>Show number</button>
      <div className="max-w-sm rounded overflow-hidden shadow-lg bg-lime-100 m-8 p-8">
        <Todos count={clicks} />
      </div>
    </>
  );
}

export default App;
